# Backend Challange

API Rest to StarWars Planets

## Features

- Add a planet
- Show planets
- Find by planet name
- Find by planet ID
- Delete a planet

## API


|VERB     | resource                      | description                        |
|:--------|:------------------------------|:-----------------------------------|
|`GET`    | `/planets`                    | returns a list of all planets      |
|`GET`    | `/planets/{id}`               | return a planet with id associated |
|`SEARCH` | `/planets/search?name={name}` | find a planet by name              |
|`POST`   | `/planets`                    | create a planet                    |
|`DELETE` | `/planets/{id}`               | remove a planet with id associated |


## Running the application

```sh
$ mvn spring-boot:run
```

