package com.b2w.starwars.swapi.client;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.b2w.starwars.swapi.client.models.Search;

@Service
public class StarWarsApi {

	private final static String ENDPOINT = "https://swapi.co/api";
	private final static String PLANETS_RESOURCE = "/planets/?search={name}";
	
	private final RestTemplate restTemplate;
    
	public StarWarsApi(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
	
	public int getNumberOfAppearancesInFilms(String planetName) {
		List<String> films = getFilmsListofPlanet(planetName);
		if (films == null) {
			return 0;
		}
		return films.size();
	}
	
	public List<String> getFilmsListofPlanet(String planetName) {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("User-Agent", "StarWarsApiClient");
		HttpEntity<String> entity = new HttpEntity<String>(headers);
    	ResponseEntity<Search> response = restTemplate.exchange(ENDPOINT + PLANETS_RESOURCE, HttpMethod.GET, entity, Search.class, planetName);
    			
    	if  (HttpStatus.valueOf(response.getStatusCodeValue()).is2xxSuccessful() && response.getBody().count > 0) {
    		return response.getBody().results.get(0).filmsUrls;
    	}
    	
    	return null;
    }
}
