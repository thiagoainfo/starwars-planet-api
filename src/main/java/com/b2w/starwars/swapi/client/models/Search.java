package com.b2w.starwars.swapi.client.models;

import java.io.Serializable;
import java.util.List;

public class Search implements Serializable {

	public int count; 
	public String next; 
	public String previous; 
	public List<Planet> results;
}
