package com.b2w.starwars.api.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.b2w.starwars.api.json.request.AddPlanetRequest;
import com.b2w.starwars.api.services.PlanetService;

import com.b2w.starwars.domain.planet.Planet;
import com.b2w.starwars.domain.planet.exceptions.PlanetException;
import com.b2w.starwars.domain.planet.exceptions.PlanetNotFoundException;

@RestController
@RequestMapping("/planets")
public class PlanetController {
	
	@Autowired
	PlanetService planetService;

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Planet addPlanet(@RequestBody @Validated AddPlanetRequest request) throws PlanetException {
		
		return planetService.createPlanet(request.getName(), request.getClimate(), request.getTerrain());		
	}
	
	@DeleteMapping(value="/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> delete(@PathVariable String id) throws PlanetException {
		
		planetService.removePlanet(id);
		
		return ResponseEntity.noContent().build();
	}
	
	@GetMapping(value="/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Planet findById(@PathVariable String id) throws PlanetNotFoundException {
		return planetService.getById(id);
	}
	
	@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<Planet> all() {
		return planetService.getAll();
	}
	
	@GetMapping(value = "/search", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Planet findByName(@RequestParam(value = "name", required=true) String name) throws PlanetNotFoundException {
		return planetService.findByName(name);
	}
}