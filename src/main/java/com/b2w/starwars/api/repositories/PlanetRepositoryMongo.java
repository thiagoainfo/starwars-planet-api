package com.b2w.starwars.api.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.b2w.starwars.domain.planet.Planet;

public interface PlanetRepositoryMongo extends MongoRepository<Planet, String> {
	
	public Planet findByName(String planetName);

}
