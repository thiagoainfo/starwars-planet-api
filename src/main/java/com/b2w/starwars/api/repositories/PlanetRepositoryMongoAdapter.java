package com.b2w.starwars.api.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import com.b2w.starwars.domain.planet.Planet;
import com.b2w.starwars.domain.planet.exceptions.PlanetNotFoundException;
import com.b2w.starwars.domain.planet.repositories.PlanetRepository;

public class PlanetRepositoryMongoAdapter implements PlanetRepository {

	@Autowired
	PlanetRepositoryMongo planetRepositoryMongo;
	
	@Override
	public Planet find(String id) throws PlanetNotFoundException {
		Optional<Planet> planet = planetRepositoryMongo.findById(id);
		return planet.get();
	}

	@Override
	public Planet findByName(String name) throws PlanetNotFoundException {
		Planet planet = planetRepositoryMongo.findByName(name);
		if (planet == null) {
			throw PlanetNotFoundException.byName(name);
		}
		
		return planet;
	}

	@Override
	public List<Planet> all() {
		return planetRepositoryMongo.findAll();
	}

	@Override
	public Planet add(Planet planet) {
		return planetRepositoryMongo.save(planet);
	}

	@Override
	public void delete(Planet planet) {
		planetRepositoryMongo.deleteById(planet.getId());
	}

}
