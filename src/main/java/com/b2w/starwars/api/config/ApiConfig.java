package com.b2w.starwars.api.config;

import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.b2w.starwars.api.repositories.PlanetRepositoryMongoAdapter;
import com.b2w.starwars.domain.planet.repositories.PlanetRepository;

@Configuration
public class ApiConfig {
	
	@Bean
	public PlanetRepository planetRepository() {
		return new PlanetRepositoryMongoAdapter();
	}
	
	@Bean
	public PoolingHttpClientConnectionManager poolingHttpClientConnectionManager() {
	    PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
	    connectionManager.setMaxTotal(1000);
	    connectionManager.setDefaultMaxPerRoute(100);
	    return connectionManager;
	}

	@Bean
	public RequestConfig requestConfig() {
	    RequestConfig requestConfig = RequestConfig.custom()
	        .setConnectionRequestTimeout(1000)
	        .setConnectTimeout(1000)
	        .setSocketTimeout(1000)
	        .build();
	    return requestConfig;
	}

	@Bean
	public CloseableHttpClient httpClient(PoolingHttpClientConnectionManager poolingHttpClientConnectionManager, RequestConfig requestConfig) {
	    CloseableHttpClient httpClient = HttpClientBuilder
	        .create()
	        .setConnectionManager(poolingHttpClientConnectionManager)
	        .setDefaultRequestConfig(requestConfig)
	        .build();
	    return httpClient;
	}

	@Bean
	public RestTemplate restTemplate(HttpClient httpClient) {
	    HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
	    requestFactory.setHttpClient(httpClient);
	    return new RestTemplate(requestFactory);
	}
}