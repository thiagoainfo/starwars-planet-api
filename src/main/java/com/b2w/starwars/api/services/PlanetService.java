package com.b2w.starwars.api.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.b2w.starwars.api.json.request.AddPlanetRequest;
import com.b2w.starwars.domain.planet.Factory;
import com.b2w.starwars.domain.planet.Planet;
import com.b2w.starwars.domain.planet.actions.Action;
import com.b2w.starwars.domain.planet.actions.AddPlanet;
import com.b2w.starwars.domain.planet.actions.DeletePlanet;
import com.b2w.starwars.domain.planet.exceptions.PlanetException;
import com.b2w.starwars.domain.planet.exceptions.PlanetNotFoundException;
import com.b2w.starwars.domain.planet.intentions.AddPlanetIntention;
import com.b2w.starwars.domain.planet.intentions.DeletePlanetIntention;
import com.b2w.starwars.domain.planet.intentions.Intention;
import com.b2w.starwars.domain.planet.repositories.PlanetRepository;
import com.b2w.starwars.swapi.client.StarWarsApi;

@Service
public class PlanetService {
	
	@Autowired
	StarWarsApi clientSWApi;
	
	@Autowired
	PlanetRepository repository;

	public Planet createPlanet(String name, String climate, String terrain) throws PlanetException {		
		int numberOfAppearancesInFilms = clientSWApi.getNumberOfAppearancesInFilms(name);
		
		Intention intention = new AddPlanetIntention(name, climate, terrain, numberOfAppearancesInFilms);
		
		Action action = new AddPlanet(repository, new Factory());				
		return action.execute(intention);
	}
	
	public void removePlanet(String id) throws PlanetException {
		Intention intention = new DeletePlanetIntention(id);
		Action action = new DeletePlanet(repository);

		action.execute(intention);
	}
	
	public List<Planet> getAll() {
		return repository.all();
	}
	
	public Planet getById(String id) throws PlanetNotFoundException {
		return repository.find(id);
	}
	
	public Planet findByName(String name) throws PlanetNotFoundException {
		return repository.findByName(name);
	}
}
