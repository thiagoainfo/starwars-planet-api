package com.b2w.starwars.api.json.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ApiErrorResponse {

    private Integer httpStatusCode;
    private String errorCode;
    private String message;
    private List<ValidationError> validationErrors;

    public ApiErrorResponse() {}

    public ApiErrorResponse(int errorCode, String message) {
        this.httpStatusCode = errorCode;
        this.errorCode = String.valueOf(errorCode);
        this.message = message;
    }

    public static ApiErrorResponse createWithValidationErrors(int errorCode, String message, List<ValidationError> validationErrors) {
        final ApiErrorResponse apiErrorResponse = new ApiErrorResponse();

        apiErrorResponse.httpStatusCode = errorCode;
        apiErrorResponse.errorCode = String.valueOf(errorCode);
        apiErrorResponse.message = message;
        apiErrorResponse.validationErrors = validationErrors;

        return apiErrorResponse;
    }

    public Integer getHttpStatusCode() {
        return httpStatusCode;
    }

    public void setHttpStatusCode(Integer httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ValidationError> getValidationErrors() {
        return validationErrors;
    }

    public void setValidationErrors(List<ValidationError> validationErrors) {
        this.validationErrors = validationErrors;
    }
}
