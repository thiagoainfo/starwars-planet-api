package com.b2w.starwars.api.json.response;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ValidationError {

	private String fieldName;
	private String fieldValue;
	private RestrictionType restrictionType;
	private String message;

	public ValidationError() {}

	public static enum RestrictionType {
		REQUIRED("required"), MATCH("match"), UNIQUE("unique"), INVALID("invalid"), BUSINESS("business"), 
		MIN_LENGTH("minLength"), MAX_LENGTH("maxLength"), RANGE("range"), EXACT_LENGTH("exactLength"), 
		GREATER_THAN("greaterThan"), LESS_THAN("lessThan"), ALPHA("alpha"), ALPHANUMERIC("alphaNumeric"), 
		NUMERIC("numeric"), INTEGER("integer"), DECIMAL("decimal"), BOOLEAN("boolean"), DATE_TIME("dateTime");

		private RestrictionType(String type) {
		}
	}

	public ValidationError(String fieldName, String fieldValue, RestrictionType restriction, String message) {
		this.fieldName = fieldName;
		this.fieldValue = fieldValue;
		this.message = message;
		restrictionType = restriction;
	}

	public String getFieldName() {
		return fieldName;
	}

	public String getFieldValue() {
		return fieldValue;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public RestrictionType getRestrictionType() {
		return restrictionType;
	}

	public void setRestrictionType(RestrictionType restrictionType) {
		this.restrictionType = restrictionType;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean equals(Object obj) {
		boolean isEqual = false;
		if (obj != null) {
			if (obj == this) {
				isEqual = true;
			} else if ((obj instanceof ValidationError)) {
				ValidationError error = (ValidationError) obj;
				isEqual = (fieldNameIsEqual(error)) && (messageIsEqual(error)) && (restrictionTypeIsEqual(error));
			}
		}

		return isEqual;
	}

	private boolean messageIsEqual(ValidationError error) {
		return ((restrictionType != null) && (restrictionType.equals(error.getRestrictionType())))
				|| ((restrictionType == null) && (error.getRestrictionType() == null));
	}

	private boolean restrictionTypeIsEqual(ValidationError error) {
		return ((message != null) && (message.equals(error.getMessage())))
				|| ((message == null) && (error.getMessage() == null));
	}

	private boolean fieldNameIsEqual(ValidationError error) {
		return ((fieldName != null) && (fieldName.equals(error.getFieldName())))
				|| ((fieldName == null) && (error.getFieldName() == null));
	}

	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("\n\tValidationError:");
		builder.append("\n\t fieldName......:[");
		builder.append(getFieldName());
		builder.append("\n\t fieldValue......:[");
		builder.append(getFieldValue());
		builder.append("],\n\t restrictionType:[");
		builder.append(getRestrictionType());
		builder.append("],\n\t message........:[");
		builder.append(getMessage());
		builder.append("]");
		return builder.toString();
	}
}
