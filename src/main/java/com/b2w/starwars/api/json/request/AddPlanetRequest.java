package com.b2w.starwars.api.json.request;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class AddPlanetRequest {

	@NotBlank(message = "The name is required.")
	private String name;
	@NotBlank(message = "The climate is required.")
	private String climate;
	@NotBlank(message = "The terrain is required.")
	private String terrain;
}
