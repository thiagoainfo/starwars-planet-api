package com.b2w.starwars.domain.planet.intentions;

public interface IdentifiedIntention extends Intention {

	public String getId();
}
