package com.b2w.starwars.domain.planet.intentions;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class AddPlanetIntention implements Intention {

	private String name;
	private String climate;
	private String terrain;
	private int numberOfAppearancesInFilms;
}
