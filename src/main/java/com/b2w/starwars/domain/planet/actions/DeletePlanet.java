package com.b2w.starwars.domain.planet.actions;

import com.b2w.starwars.domain.planet.Planet;
import com.b2w.starwars.domain.planet.exceptions.PlanetException;
import com.b2w.starwars.domain.planet.intentions.IdentifiedIntention;
import com.b2w.starwars.domain.planet.intentions.Intention;
import com.b2w.starwars.domain.planet.repositories.PlanetRepository;

public class DeletePlanet implements Action {
	
	private PlanetRepository repository;
	
	public DeletePlanet(PlanetRepository repository) {
		this.repository = repository;
	}

	@Override
	public Planet execute(Intention intention) throws PlanetException {
		return deletePlanet((IdentifiedIntention) intention);
	}
	
	private Planet deletePlanet(IdentifiedIntention intention) throws PlanetException {
		
		Planet planet = repository.find(intention.getId());
		
		repository.delete(planet);
		
		return planet;
	}

}
