package com.b2w.starwars.domain.planet.intentions;

public class DeletePlanetIntention implements IdentifiedIntention {

	private String id;
	
	public DeletePlanetIntention(String id) {
		this.id = id;
	}
	
	@Override
	public String getId() {
		return this.id;
	}
}
