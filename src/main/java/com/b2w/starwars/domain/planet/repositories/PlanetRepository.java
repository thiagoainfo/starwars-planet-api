package com.b2w.starwars.domain.planet.repositories;

import java.util.List;

import com.b2w.starwars.domain.planet.Planet;
import com.b2w.starwars.domain.planet.exceptions.PlanetNotFoundException;

public interface PlanetRepository {
	
	public Planet find(String id) throws PlanetNotFoundException;
	
	public Planet findByName(String name) throws PlanetNotFoundException;
	
	public List<Planet> all();
	
	public Planet add(Planet planet);
	
	public void delete(Planet planet);
}
