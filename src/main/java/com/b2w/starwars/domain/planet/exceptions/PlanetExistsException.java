package com.b2w.starwars.domain.planet.exceptions;

public class PlanetExistsException extends PlanetException {

	PlanetExistsException(String message) {
		super(message);
	}
	
	public static PlanetExistsException withName(String name) {
		return new PlanetExistsException("The planet " + name + " already exists");
	}
}
