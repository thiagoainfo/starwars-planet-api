package com.b2w.starwars.domain.planet.exceptions;

public class PlanetNotFoundException extends PlanetException {

	public PlanetNotFoundException() {
		super();
	}
	
	public PlanetNotFoundException(String message) {
		super(message);
	}

	public static PlanetNotFoundException byName(String name) {
		return new PlanetNotFoundException("The planet("+name+") was not found.");
	}
	
	public static PlanetNotFoundException byIdentifier(String id) {
		return PlanetNotFoundException.byName(id);
	}	
}
