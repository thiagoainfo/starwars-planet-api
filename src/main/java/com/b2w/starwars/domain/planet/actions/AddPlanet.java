package com.b2w.starwars.domain.planet.actions;

import java.util.List;

import com.b2w.starwars.domain.planet.Factory;
import com.b2w.starwars.domain.planet.Planet;
import com.b2w.starwars.domain.planet.exceptions.PlanetException;
import com.b2w.starwars.domain.planet.exceptions.PlanetNotFoundException;
import com.b2w.starwars.domain.planet.intentions.AddPlanetIntention;
import com.b2w.starwars.domain.planet.intentions.Intention;
import com.b2w.starwars.domain.planet.repositories.PlanetRepository;

public class AddPlanet implements Action {
	
	private PlanetRepository repository;
	private Factory factory;
	
	public AddPlanet(PlanetRepository repository, Factory factory) {
		this.repository = repository;
		this.factory = factory;
	}

	@Override
	public Planet execute(Intention intention) throws PlanetException {
		Planet planet;
		
		AddPlanetIntention addIntention = (AddPlanetIntention) intention;
		
		try {
			planet = repository.findByName(addIntention.getName());
			
			throw PlanetException.existsWithName(addIntention.getName());
		} catch (PlanetNotFoundException e) {
			// do nothing
		}
		
		planet = factory.createFromIntention((AddPlanetIntention) intention);
		
		return repository.add(planet);
	}

}
