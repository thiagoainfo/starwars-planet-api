package com.b2w.starwars.domain.planet.actions;

import com.b2w.starwars.domain.planet.Planet;
import com.b2w.starwars.domain.planet.exceptions.PlanetException;
import com.b2w.starwars.domain.planet.intentions.Intention;

public interface Action {

	public Planet execute(Intention intention) throws PlanetException;
}
