package com.b2w.starwars.domain.planet;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class Planet {

	private String id;
	private String name;
	private String climate;
	private String terrain;
	private int numberOfAppearancesInFilms;
}
