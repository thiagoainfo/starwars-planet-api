package com.b2w.starwars.domain.planet.exceptions;

public class PlanetException extends Exception {

	public PlanetException() {
		super();
	}
	
	public PlanetException(String message) {
		super(message);
	}
	
	public static PlanetExistsException existsWithName(String name) {
		return PlanetExistsException.withName(name);
	}
}
