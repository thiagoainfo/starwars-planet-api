package com.b2w.starwars.domain.planet;

import com.b2w.starwars.domain.planet.intentions.AddPlanetIntention;

public class Factory {

	public Planet createFromIntention(AddPlanetIntention addPlanetIntention) {
		return Planet.builder()				
				.name(addPlanetIntention.getName())
				.climate(addPlanetIntention.getClimate())
				.terrain(addPlanetIntention.getTerrain())
				.numberOfAppearancesInFilms(addPlanetIntention.getNumberOfAppearancesInFilms())
				.build();
	}
}
