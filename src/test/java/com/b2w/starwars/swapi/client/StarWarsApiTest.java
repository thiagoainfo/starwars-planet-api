package com.b2w.starwars.swapi.client;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class StarWarsApiTest {

	@Autowired
	private StarWarsApi swApi;
	
	@Test
	public void shouldReturnFilmsListForPlanet() {
		List<String> films = swApi.getFilmsListofPlanet("Alderaan");
		assertThat(films).isNotEmpty();
	}
	
	@Test
	public void shouldNotReturnFilmsListForPlanet() {
		List<String> films = swApi.getFilmsListofPlanet("Alderaan !!!");
		assertNull(films);
	}
	
	@Test
	public void shouldReturnAppearancesInFilmsGreaterThanZero() {
		int numberOfAppearancesInFilms = swApi.getNumberOfAppearancesInFilms("Alderaan");
		assertNotEquals(0, numberOfAppearancesInFilms);
	}
	
	@Test
	public void shouldReturnAppearancesInFilmsEqualsZero() {
		int numberOfAppearancesInFilms = swApi.getNumberOfAppearancesInFilms("Alderaan !!");
		assertEquals(0, numberOfAppearancesInFilms);
	}
}
