package com.b2w.starwars.api.controller;

import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.b2w.starwars.api.json.request.AddPlanetRequest;
import com.b2w.starwars.api.services.PlanetService;
import com.b2w.starwars.domain.planet.Planet;
import com.b2w.starwars.domain.planet.exceptions.PlanetException;
import com.b2w.starwars.domain.planet.exceptions.PlanetNotFoundException;
import com.b2w.starwars.domain.planet.repositories.PlanetRepository;
import com.b2w.starwars.swapi.client.StarWarsApi;
import com.fasterxml.jackson.databind.ObjectMapper;



@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class PlanetControllerTest {
	
	@Autowired
    private MockMvc mvc;
	
	@MockBean
	PlanetService service;
		
	ObjectMapper mapper = new ObjectMapper();

	@Test
	public void testCreateANewPlanetSuccessfully() throws Exception {
		
		AddPlanetRequest payload = AddPlanetRequest.builder()
				.name("Hoth")
				.climate("frozen")
				.terrain("tundra, ice caves, mountain ranges")
				.build();
		
		when(service.createPlanet("Hoth", "frozen", "tundra, ice caves, mountain ranges"))
			.thenReturn(new Planet("112233", "Hoth", "frozen", "tundra, ice caves, mountain ranges", 10));
		
		mvc.perform(post("/planets").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(payload)))
			.andExpect(status().isOk())
			.andExpect(jsonPath("id", is("112233")))
			.andExpect(jsonPath("name", is("Hoth")))
			.andExpect(jsonPath("climate", is("frozen")))
			.andExpect(jsonPath("terrain", is("tundra, ice caves, mountain ranges")))
			.andExpect(jsonPath("numberOfAppearancesInFilms", is(10)));
	}
	
	@Test
	public void testCreateAPlanetWithoutName() throws Exception {
		
		AddPlanetRequest payload = AddPlanetRequest.builder()				
				.climate("frozen")
				.terrain("tundra, ice caves, mountain ranges")
				.build();
		
		when(service.createPlanet("Hoth", "frozen", "tundra, ice caves, mountain ranges"))
			.thenReturn(new Planet("112233", "Hoth", "frozen", "tundra, ice caves, mountain ranges", 10));
		
		mvc.perform(post("/planets").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(payload)))
			.andExpect(status().isBadRequest())
			.andExpect(jsonPath("httpStatusCode", is(HttpStatus.BAD_REQUEST.value())));
	}
	
	@Test
	public void testCreateAPlanetWithoutClimate() throws Exception {
		
		AddPlanetRequest payload = AddPlanetRequest.builder()
				.name("Hoth")				
				.terrain("tundra, ice caves, mountain ranges")
				.build();
		
		when(service.createPlanet("Hoth", "frozen", "tundra, ice caves, mountain ranges"))
			.thenReturn(new Planet("112233", "Hoth", "frozen", "tundra, ice caves, mountain ranges", 10));
		
		mvc.perform(post("/planets").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(payload)))
			.andExpect(status().isBadRequest())
			.andExpect(jsonPath("httpStatusCode", is(HttpStatus.BAD_REQUEST.value())));
	}
	
	@Test
	public void testCreateAPlanetWithoutTerrain() throws Exception {
		
		AddPlanetRequest payload = AddPlanetRequest.builder()
				.name("Hoth")
				.climate("frozen")				
				.build();
		
		when(service.createPlanet("Hoth", "frozen", "tundra, ice caves, mountain ranges"))
			.thenReturn(new Planet("112233", "Hoth", "frozen", "tundra, ice caves, mountain ranges", 10));
		
		mvc.perform(post("/planets").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(payload)))
			.andExpect(status().isBadRequest())
			.andExpect(jsonPath("httpStatusCode", is(HttpStatus.BAD_REQUEST.value())));
	}
	
	@Test
	public void testDeleteAPlanetSuccessfully() throws Exception {
		
		mvc.perform(delete("/planets/{id}", "5bda971cd2023d01b2608979"))
			.andExpect(status().isNoContent());
	}
	
	@Test
	public void testDeleteAPlanetNonExistent() throws Exception {
		
		doThrow(new PlanetException()).when(service).removePlanet("5bda971cd2023d01b2608979");
		
		mvc.perform(delete("/planets/{id}", "5bda971cd2023d01b2608979"))
			.andExpect(status().is5xxServerError());
	}
	
	@Test
	public void testSearchAPlanetSuccessfully() throws Exception {
		
		when(service.findByName("Hoth"))
			.thenReturn(new Planet("112233", "Hoth", "frozen", "tundra, ice caves, mountain ranges", 10));
		
		mvc.perform(get("/planets/search?name={name}", "Hoth"))
			.andExpect(status().isOk())
			.andExpect(jsonPath("id", is("112233")))
			.andExpect(jsonPath("name", is("Hoth")))
			.andExpect(jsonPath("climate", is("frozen")))
			.andExpect(jsonPath("terrain", is("tundra, ice caves, mountain ranges")))
			.andExpect(jsonPath("numberOfAppearancesInFilms", is(10)));
	}
	
	@Test
	public void testFindAPlanetById() throws Exception {
		
		when(service.getById("112233"))
			.thenReturn(new Planet("112233", "Hoth", "frozen", "tundra, ice caves, mountain ranges", 10));
	
		mvc.perform(get("/planets/{id}", "112233"))
			.andExpect(status().isOk())
			.andExpect(jsonPath("id", is("112233")))
			.andExpect(jsonPath("name", is("Hoth")))
			.andExpect(jsonPath("climate", is("frozen")))
			.andExpect(jsonPath("terrain", is("tundra, ice caves, mountain ranges")))
			.andExpect(jsonPath("numberOfAppearancesInFilms", is(10)));
		}
}
