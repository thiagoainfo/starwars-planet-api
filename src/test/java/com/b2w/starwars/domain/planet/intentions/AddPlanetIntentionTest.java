package com.b2w.starwars.domain.planet.intentions;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.b2w.starwars.domain.planet.intentions.AddPlanetIntention;

public class AddPlanetIntentionTest {
	
	@Test
	public void testShouldBeInstanciate() {
		AddPlanetIntention intention = new AddPlanetIntention("Kamino", "temperate", "ocean", 2);
		assertEquals("Kamino", intention.getName());
		assertEquals("temperate", intention.getClimate());
		assertEquals("ocean", intention.getTerrain());
		assertEquals(2, intention.getNumberOfAppearancesInFilms());
	}

}
