package com.b2w.starwars.domain.planet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.b2w.starwars.domain.planet.Factory;
import com.b2w.starwars.domain.planet.Planet;
import com.b2w.starwars.domain.planet.intentions.AddPlanetIntention;

public class FactoryTest {
	
	@Test
	public void testCreatePlanetFromIntention() {
		
		Factory factory = new Factory();
		
		Planet planet = factory.createFromIntention(new AddPlanetIntention("Yavin IV", "temperate, tropical", "jungle, rainforests", 1));
		assertNotNull(planet);
		assertEquals("Yavin IV", planet.getName());
		assertEquals("temperate, tropical", planet.getClimate());
		assertEquals("jungle, rainforests", planet.getTerrain());
		assertEquals(1, planet.getNumberOfAppearancesInFilms());
	}
}
