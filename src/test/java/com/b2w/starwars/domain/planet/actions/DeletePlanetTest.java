package com.b2w.starwars.domain.planet.actions;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;

import com.b2w.starwars.domain.planet.Planet;
import com.b2w.starwars.domain.planet.actions.DeletePlanet;
import com.b2w.starwars.domain.planet.exceptions.PlanetException;
import com.b2w.starwars.domain.planet.exceptions.PlanetExistsException;
import com.b2w.starwars.domain.planet.exceptions.PlanetNotFoundException;
import com.b2w.starwars.domain.planet.intentions.DeletePlanetIntention;
import com.b2w.starwars.domain.planet.repositories.PlanetRepository;

public class DeletePlanetTest {
	
	@Test
	public void testDeleteAPlanetSuccessful() throws PlanetException {
		PlanetRepository repository = mock(PlanetRepository.class);
		when(repository.find("1")).thenReturn(new Planet("1", "Stewjon", "temperate", "grass", 0));
		
		DeletePlanet action = new DeletePlanet(repository);
		
		DeletePlanetIntention intention = new DeletePlanetIntention("1");
		action.execute(intention);
		
		assertTrue(true);
	}
	
}
