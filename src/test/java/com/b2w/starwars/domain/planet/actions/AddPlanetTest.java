package com.b2w.starwars.domain.planet.actions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;

import org.junit.Test;

import com.b2w.starwars.domain.planet.Factory;
import com.b2w.starwars.domain.planet.Planet;
import com.b2w.starwars.domain.planet.actions.Action;
import com.b2w.starwars.domain.planet.actions.AddPlanet;
import com.b2w.starwars.domain.planet.exceptions.PlanetException;
import com.b2w.starwars.domain.planet.exceptions.PlanetExistsException;
import com.b2w.starwars.domain.planet.exceptions.PlanetNotFoundException;
import com.b2w.starwars.domain.planet.intentions.AddPlanetIntention;
import com.b2w.starwars.domain.planet.repositories.PlanetRepository;

public class AddPlanetTest {

	@Test
	public void testAddANewPlanetSuccessful() throws PlanetNotFoundException {
		
		PlanetRepository repository = mock(PlanetRepository.class);		
		when(repository.findByName("Stewjon")).thenThrow(new PlanetNotFoundException());
		when(repository.add(any(Planet.class))).thenReturn(new Planet("1", "Stewjon", "temperate", "grass", 10));
		
		Action action = new AddPlanet(repository, new Factory());
		
		AddPlanetIntention intention = new AddPlanetIntention("Stewjon", "temperate", "grass", 10);
		try {
			Planet planet = action.execute(intention);
			
			assertEquals("Stewjon", planet.getName());
			assertEquals("temperate", planet.getClimate());
			assertEquals("grass", planet.getTerrain());
			assertEquals(10, planet.getNumberOfAppearancesInFilms());
		} catch (PlanetException e) {
			fail();
		}
	}
	
	@Test(expected = PlanetExistsException.class)
	public void testAddANewPlanetFailed() throws PlanetException {
		PlanetRepository repository = mock(PlanetRepository.class);		
		when(repository.findByName("Stewjon")).thenReturn(new Planet("1", "Stewjon", "temperate", "grass", 9));
		
		Action action = new AddPlanet(repository, new Factory());
		AddPlanetIntention intention = new AddPlanetIntention("Stewjon", "temperate", "grass", 10);		
		Planet planet = action.execute(intention);
	}
}
