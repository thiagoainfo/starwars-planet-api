package com.b2w.starwars.domain.planet;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.b2w.starwars.domain.planet.Planet;

public class PlanetTest {

	@Test
	public void testShouldBeInstanciate() {
		Planet planet = Planet.builder()
				.name("Hoth")
				.climate("frozen")
				.terrain("tundra, ice caves, mountain ranges")
				.numberOfAppearancesInFilms(8)
				.build();
		assertEquals("Hoth", planet.getName());
		assertEquals("frozen", planet.getClimate());
		assertEquals("tundra, ice caves, mountain ranges", planet.getTerrain());
		assertEquals(8, planet.getNumberOfAppearancesInFilms());
	}
}
