package com.b2w.starwars.domain.planet.intentions;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.b2w.starwars.domain.planet.intentions.DeletePlanetIntention;

public class DeletePlanetIntentionTest {

	@Test
	public void testShouldBeInstanciate() {
		DeletePlanetIntention intention = new DeletePlanetIntention("1");
		assertEquals("1", intention.getId());
	}
}
